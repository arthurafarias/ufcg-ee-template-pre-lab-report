# README #

This readme will guide you to add this project to your workspace at Eclipse Platform With Egit and TeXlipse properly configured.

Install instructions about Eclipse Platform.

http://askubuntu.com/questions/26632/how-to-install-eclipse

Installing instructions about TeXlipse plugin is available at their own website.

http://texlipse.sourceforge.net/manual/installation.html

Installing instructions about TeXlipse egit plugin is available at.

http://stackoverflow.com/questions/1623586/installing-git-on-eclipse

### How do I get set up? ###

if the LaTeX distribution and the eclipse environment are installed properly, you should be able to import this git repository just by following the steps below.

1. Go to File>Import
2. Type "projects from git" in the search field.
3. Select Git>Projects from Git
4. Click on "Next >" button
5. Choose Clone URI
6. Fork this project and type the clone URL in clone URI field.
7. Put your username or passwork if applicable.
8. Click "Next >"
9. Select a preferable branch.
10. Choose an appropriate destination directory. Let anything else unchanged.
11. Click "Next >"
12. Choose Import existing Eclipse projects
13. Select ufcg-ee-template-pre-lab-report
14. Click Finish.

You should be able to build this project and see the output pdf in the Build folder.

Do not forget to change the project name by an appropriate one.

### Who do I talk to? ###

* Repo owner: Arthur Farias (arthur@afarias.org)